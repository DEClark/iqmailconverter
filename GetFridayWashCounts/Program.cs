﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Text;
using System.Threading.Tasks;

namespace GetFridayWashCounts
{
    class Program
    {
        public const string Connection = "Server = DESKTOP-GVRI33L\\SQLEXPRESS; Database = IQLog; User Id = sa; Password = !big8phasedlogix;";
        static void Main(string[] args)
        {

            //CreateMessageWithAttachment("dclark@phasedlogix.com");
            //Console.ReadLine();
            //return;

            //string Connection = "Server = (LocalDb)\\MSSQLLocalDB; Database = IQLog; User Id = admin; Password = big8phasedlogix;";
            using (var conn = new SqlConnection(Connection))
            using (var command = new SqlCommand("sp_insetSerialsfromWashCount", conn)
            {
                CommandType = CommandType.StoredProcedure
            })
            {
                conn.Open();
                command.ExecuteNonQuery();
            }
            SqlConnection sqlConnection1 = new SqlConnection(Connection);
            SqlCommand cmd = new SqlCommand();
            SqlDataReader reader;

            cmd.CommandText = "dbo.GetWashExcel";
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Connection = sqlConnection1;
            
            sqlConnection1.Open();
            string filePath = @"C:\Users\phasedlogix\Desktop\MailTasks\";

            using (System.IO.StreamWriter file =
                new System.IO.StreamWriter(Path.Combine(filePath,"WashCounts_" +System.DateTime.Now.Date.ToString("yyyy_MM_dd")+".csv"),false))
            {
                string lineBuilder = "Serial Number, Wash Count, Code";
                file.WriteLine(lineBuilder);

                reader = cmd.ExecuteReader();
                while (reader.Read())
                {
                    lineBuilder = "";
                    lineBuilder += reader[0] + ",";
                    lineBuilder += reader[1] + ",";
                    lineBuilder += reader[2] + ",";
                    file.WriteLine(lineBuilder);
                }
                // Data is accessible through the DataReader object here.
            }
            sqlConnection1.Close();

            sendMail();

            //Console.ReadLine();


            //client.Host = "smtp.dscarwash.com";
            //string domain = "dscarwash.com";
            //client.Credentials = new SmtpCredential("mailuser", "dscarwash10", domain);

        }

        public static void sendMail()
        {

            using (SqlConnection connection = new SqlConnection(Connection))
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM EmailList";
                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.KeyInfo))
                {

                    while (reader.Read())
                    {
                        Console.WriteLine((string)reader[1]);
                        CreateMessageWithAttachment((string)reader[1]);
                    }

                }


            }
        }

        public static void CreateMessageWithAttachment(string toAddress)
        {
            // Specify the file to be attached and sent.
            // This example assumes that a file named Data.xls exists in the
            // current working directory.
            string filePath = @"C:\Users\phasedlogix\Desktop\MailTasks\";
            string file = "WashCounts_" + System.DateTime.Now.Date.ToString("yyyy_MM_dd") + ".csv";
            // Create a message and set up the recipients.
            MailMessage message = new MailMessage(
               "mailuser@dscarwashonline.com",
               toAddress,
               "Wash Count Report.",
               "Wash Count Report.");

            // Create  the file attachment for this e-mail message.
            Attachment data = new Attachment(Path.Combine(filePath,file), MediaTypeNames.Application.Octet);
            // Add time stamp information for the file.
            // Add the file attachment to this e-mail message.
            message.Attachments.Add(data);

            //Send the message.
            SmtpClient client = new SmtpClient();
            // Add credentials if the SMTP server requires them.
            client.Host = "smtp1.dscarwashonline.com";
            //client.Host= "smtp.gmail.com";
            //client.Port = 587;
            client.Port = 2525;
            client.UseDefaultCredentials = false;
            client.Credentials = new NetworkCredential("mailuser@dscarwashonline.com", "dscarwash10");
            //client.Credentials = new NetworkCredential("dclark@phasedlogix.com", "sk3l3tor");
            client.EnableSsl = false;

            try
            {
                client.Send(message);
            }
            catch (Exception ex)
            {
                Console.WriteLine("Exception caught in CreateMessageWithAttachment(): {0}",
                            ex.ToString());
            }
            // Display the values in the ContentDisposition for the attachment.

            data.Dispose();
        }



    }
}
