﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;
using System.Runtime.Serialization;

namespace MailTasks.Models
{
    public class Device
    {
        [BsonId]
        public ObjectId MongoDbId { get; set; }

        [BsonElement("serial")]
        public string Serial { get; set; }

        [BsonElement("password")]
        public string Password { get; set; }

        [BsonElement("subscription")]
        public Subscription Subscription { get; set; }

        [BsonElement("hubDetails")]
        public HubDetails HubDetails { get; set; }

        [BsonElement("hasHub")]
        public bool HasHub { get; set; }
    }
}
