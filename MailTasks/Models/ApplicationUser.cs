﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System.Collections.Generic;

namespace MailTasks.Models
{
    public class ApplicationUser
    {
        [BsonId]
        public ObjectId MongoDbId { get; set; }

        [BsonElement("auth0Id")]
        public string Auth0Id { get; set; }

        [BsonElement("email")]
        public string Email { get; set; }

        [BsonElement("stripeId")]
        public string StripeId { get; set; }

        [BsonElement("devices")]
        public IList<Device> Devices { get; set; }
    }
}
