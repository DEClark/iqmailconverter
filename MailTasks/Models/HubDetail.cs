﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MailTasks.Models
{
    public class HubDetails

    { 
        [BsonElement("relayIP")]
        public string RelayIP { get; set; }
        [BsonElement("relayPort")]
        public string RelayPort { get; set; }
        [BsonElement("hubKey")]
        public string HubKey { get; set; }
    }
}
