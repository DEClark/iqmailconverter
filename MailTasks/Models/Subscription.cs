﻿using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace MailTasks.Models
{
    public class Subscription
    {
        [BsonElement("subscriptionId")]
        public string SubscriptionId { get; set; }

        [BsonElement("expiryDate")]
        public string ExpiryDate { get; set; }

        [BsonElement("isExpired")]
        public bool IsExpired { get; set; }

        [BsonElement("customerId")]
        public string CustomerId { get; set; }

    }
}