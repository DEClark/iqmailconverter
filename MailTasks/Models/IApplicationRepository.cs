﻿using MongoDB.Driver;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace MailTasks.Models
{
    /// <summary>
    /// Declares CRUD methods for retrieving information from this application's MongoDb connection.
    /// </summary>
    public interface IApplicationRepository
    {

        Task<bool> DeviceExists(string serial);

        Task<List<Device>> GetDevices();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<Device> GetDeviceBySerial(string serial);


        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<IList<Device>> AddDevice(Device device);




    }
}
