﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Driver;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace MailTasks.Models
{
    /// <summary>
    /// Implements CRUD methods to manipulate data in this application's MongoDb connection.
    /// </summary>
    public class ApplicationRepository : IApplicationRepository
    {   
        // Represents a single MongoDb connection
        private MongoClient _client;

        // Represents a databse associated with a MongoClient
        private IMongoDatabase _db;

        // Application-wide configuration object


        // Represents a single collection in an IMongoDatabase
        private IMongoCollection<ApplicationUser> _usersCollection;
        private IMongoCollection<Device> _devicesCollection;
        
        public ApplicationRepository()
        {
            // Set up a connection to the MongoDb in this class
            _client = new MongoClient("mongodb://iqnet:EuUPugJ0Y21QxIyGbp08@ds051831.mlab.com:51831/iqlogdb");
            _db = _client.GetDatabase("iqlogdb");
            _usersCollection = _db.GetCollection<ApplicationUser>("Users");
            _devicesCollection = _db.GetCollection<Device>("Devices");

            // Set up logging and configuration for the class
        }

        public async Task<bool> DeviceExists(string serial)
        {
            var filter = Builders<Device>.Filter.Where(device => device.Serial == serial);

            var result = await _devicesCollection.Find(filter).FirstOrDefaultAsync();

            if (result != null)
            {
                return true;
            }


            return false;
        }

        public async Task<List<Device>> GetDevices()
        {

            var result = _devicesCollection.AsQueryable();

            if (result != null)
            {
                return await result.ToListAsync();
            }

            return null;

        }

        /// <summary>
        /// Retrieves a single device by serial.
        /// </summary>
        /// <returns></returns>
        public async Task<Device> GetDeviceBySerial(string serial)
        {
            var filter = Builders<Device>.Filter.Eq("serial", serial);
            var result = await _devicesCollection.Find(filter).FirstOrDefaultAsync();

            if (result != null)
            {
                return result;
            }

            return null;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IList<Device>> AddDevice(Device device)
        {
            IList<Device> devices = new List<Device>();

            await _devicesCollection.InsertOneAsync(device);
            await _devicesCollection.Find(new BsonDocument()).ForEachAsync(d =>
            {
                if (d != null)
                {
                    devices.Add(d);
                }
            });

            return devices;
        }






    }
}

/** 
 * Copyright 2017 PhasedLogix, LLC. All Rights Reserved.
 */
