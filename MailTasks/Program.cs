﻿using MailTasks.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace MailTasks
{
    class Program
    {
        private static ApplicationRepository MongoRepo = new ApplicationRepository();
        static void Main(string[] args)
        {
            //UpdateMongo();

            string Connection = "Server = DESKTOP-GVRI33L\\SQLEXPRESS; Database = IQLog; User Id = sa; Password = !big8phasedlogix;";
            //string Connection = "Server = (LocalDb)\\MSSQLLocalDB; Database = IQLog; User Id = admin; Password = big8phasedlogix;";
            List<SqlCommand> WashCountList = new List<SqlCommand>();
            using (SqlConnection connection = new SqlConnection(Connection))
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM MailCount WHERE processed IS NULL OR processed = 0";
                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.KeyInfo))
                {

                    string query = "INSERT INTO dbo.WashCount (serial, count, version, countDate, mailcountid) " +
                        "VALUES (@Serial, @Count, @Version, @CountDate, @Mailcountid) ";
                    while (reader.Read())
                    {
                        using (SqlCommand cmd = new SqlCommand(query, connection))
                        {
                            // define parameters and their values
                            cmd.Parameters.Add("@Serial", SqlDbType.NVarChar, 50).Value = getSerialFromSubject((string)reader[1]);
                            cmd.Parameters.Add("@Count", SqlDbType.Int).Value = getWashCountFromSubject((string)reader[1]);
                            cmd.Parameters.Add("@Version", SqlDbType.NVarChar, 50).Value = getVersionFromBody((string)reader[2]);
                            cmd.Parameters.Add("@CountDate", SqlDbType.Date).Value = reader[3];
                            cmd.Parameters.Add("@Mailcountid", SqlDbType.Int).Value = reader[0];


                            // open connection, execute INSERT, close connection
                            WashCountList.Add(cmd);
                            //cmd.ExecuteNonQuery();
                        }

                    }

                }
                foreach (SqlCommand comm in WashCountList)
                {
                    comm.ExecuteNonQuery();
                }

                string updateProcessed = "UPDATE MailCount set processed = 1 WHERE processed IS NULL OR processed = 0";
                using (SqlCommand cmd = new SqlCommand(updateProcessed, connection))
                {
                    cmd.ExecuteNonQuery();
                }
            }

        }

        public static async void UpdateMongo()
        {
            Device devToAdd = new Device();
            //Console.WriteLine("Starting to Check");
            string Connection = "Server = DESKTOP-GVRI33L\\SQLEXPRESS; Database = IQLog; User Id = sa; Password = !big8phasedlogix;";
            //string Connection = "Server = (LocalDb)\\MSSQLLocalDB; Database = IQLog; User Id = admin; Password = big8phasedlogix;";
            using (SqlConnection connection = new SqlConnection(Connection))
            using (SqlCommand command = connection.CreateCommand())
            {
                command.CommandText = "SELECT * FROM IQList";
                connection.Open();

                using (SqlDataReader reader = command.ExecuteReader(CommandBehavior.KeyInfo))
                {

                    while (reader.Read())
                    {
                        if (await MongoRepo.DeviceExists(((string)reader[0]).ToUpper()))
                        {
                            //Console.WriteLine((string)reader[0]+": Exists");
                        }
                        else
                        {
                            devToAdd.Serial = ((string)reader[0]).ToUpper();
                            MongoRepo.AddDevice(devToAdd);
                            devToAdd = new Device();
                            //Console.WriteLine((string)reader[0] + ": Does Not Exist");
                        }


                    }

                }

            }
        }
        private static void InsertWashCount(string connectionString, string serial, int count, string version, DateTime countDate,
                        int mailcountid)
        {
            // define INSERT query with parameters
            string query = "INSERT INTO dbo.WashCount (serial, count, version, countDate, mailcountid) " +
                           "VALUES (@Serial, @Count, @Version, @CountDate, @Mailcountid) ";

            // create connection and command
            using (SqlConnection cn = new SqlConnection(connectionString))
            using (SqlCommand cmd = new SqlCommand(query, cn))
            {
                // define parameters and their values
                cmd.Parameters.Add("@Serial", SqlDbType.NVarChar, 50).Value = serial;
                cmd.Parameters.Add("@Count", SqlDbType.Int).Value = count;
                cmd.Parameters.Add("@Version", SqlDbType.NVarChar, 50).Value = version;
                cmd.Parameters.Add("@CountDate", SqlDbType.Date).Value = countDate;
                cmd.Parameters.Add("@Mailcountid", SqlDbType.Int).Value = mailcountid;


                // open connection, execute INSERT, close connection
                cn.Open();
                cmd.ExecuteNonQuery();
                cn.Close();
            }
        }


        public static string getVersionFromBody(string input)
        {
            string pattern = @"\d+[.]\d+[.]\d+[.]\d+";
            //            string input = @"Version:  2.4.0.1
            //Wash Count: 42010
            //Date and Time: 8/28/2017 3:59:21 AM




            //";

            foreach (Match m in Regex.Matches(input, pattern))
            {
                return m.Value;
                //Console.WriteLine("'{0}' found at index {1}.", m.Value, m.Index);
            }
            return "";
        }

        public static int getWashCountFromSubject(string input)
        {
            string pattern = @"Counts -\s(\d+)";
            //            string input = @"BRIDGEPORT: IQ116ST106 Counts - 12246
            //";

            foreach (Match m in Regex.Matches(input, pattern))
            {

                return int.Parse(m.Value.Replace("Counts - ", ""));
                //Console.WriteLine("'{0}' found at index {1}.", m.Value, m.Index);
            }
            return 0;
        }

        public static string getSerialFromSubject(string input)
        {
            string pattern = @"[I][Q]\d+[S,T][T,F]\d+|[I][Q][M][A][X]\d+|[I][Q][X][L]\d+|[I][Q]\d+[S][T][S][B]\d+";
            //[I][Q]\d+[S,T][T,F]\d+|[I][Q][M][A][X]\d+|[I][Q][X][L]\d+|[I][Q]\d+[S][T][S][B]\d+
            //            string input = @"BRIDGEPORT: IQ116ST106 Counts - 12246
            //";

            foreach (Match m in Regex.Matches(input, pattern))
            {
                return m.Value;
                //Console.WriteLine("'{0}' found at index {1}.", m.Value, m.Index);
            }
            return "";
        }
    }
}
